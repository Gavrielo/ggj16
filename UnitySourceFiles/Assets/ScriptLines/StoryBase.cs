﻿using UnityEngine;
using System.Collections;

public class StoryBase: MonoBehaviour
{
    public AutoType TypeTarget;
    public string BaseText { get; set; }
    protected int PresentTime = 0;
    protected int EndTime { get; set; }
    public string CorrectInput { get; set; }
    public string CorrectInput2 { get; set; }
    public int Status = 1; //3 = completed
    public int Choice;
    protected string ChoiceR;
    public GameObject TargetObj;
    public MusicManager MusicTarget;
    public SFXManager SFXTarget;

    public StoryBase(string baseText, string correctInput, string correctInput2)
    {
        //Sets time from inherited class
        this.BaseText = baseText;
        this.CorrectInput = correctInput;
        this.CorrectInput2 = correctInput2;
    }

    void Start()
    {
        TypeTarget = (AutoType)FindObjectOfType(typeof(AutoType));
        MusicTarget = (MusicManager)FindObjectOfType(typeof(MusicManager));
        SFXTarget = (SFXManager)FindObjectOfType(typeof(SFXManager));
    }

    public virtual void CallProgress()
    {

    }

    public virtual void StartOfLine()
    {

    }

    public virtual IEnumerator LoadNewLine()
    {
        TypeTarget.MainText.text = " ";
        yield return new WaitForSeconds(3f);
        TypeTarget.StoryPos++;
        TypeTarget.InitTypeText();
        enabled = false;
    }
}
