﻿using UnityEngine;
using System.Collections;

public class C1P07 : StoryBase
{

    // Use this for initialization
    public C1P07() : base(" Something is waking. Can you      feel it?", "Y", "N")
    {

    }
    

    // Update is called once per frame
    public override void CallProgress()
    {
        TargetObj.GetComponent<Animation>().Play("ShakeCamera");
        SFXTarget.GetComponent<SFXManager>().PlayEfc(1);
        MusicTarget.GetComponent<MusicManager>().StopMusic();
        MusicTarget.GetComponent<MusicManager>().PlayEfc(2);
        Status = 2;
    }

    void Update()
    {
        if (Status == 2)
        {
            if (Input.anyKeyDown)
            {
                // BaseText = "[footsteps]";
                Status = 3;
               // TypeTarget.InitTypeText();
                StartCoroutine(LoadNewLine());
            }
        }
    }    
    
}
