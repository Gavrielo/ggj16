﻿using UnityEngine;
using System.Collections;

public class C1P06b : StoryBase
{

    // Use this for initialization
    public C1P06b() : base("Do you ever feel like just another meaningless cog in the machine? (Y/N)", "Y", "N")
    {

    }

    // Update is called once per frame
    public override void CallProgress()
    {
        Status = 2;
    }

    void Update()
    {
        if (Status == 2)
        {
            if (Input.GetKeyDown(KeyCode.Y))
            {
                BaseText = "  ";
               // TargetObj.SetActive(true);
                Status = 3;
                TargetObj.SetActive(true);
                TypeTarget.InitTypeText();
                StartCoroutine(LoadNewLine());
            }
            else if (Input.GetKeyDown(KeyCode.N))
            {
                BaseText = "  ";
                //TargetObj.SetActive(true);
                Status = 3;
                TargetObj.SetActive(true);
                TypeTarget.InitTypeText();
                StartCoroutine(LoadNewLine());
            }
        }

    }


}
