﻿using UnityEngine;
using System.Collections;

public class C1P04 : StoryBase
{

    // Use this for initialization
    public C1P04() : base("Do you ever get the feeling you're being watched? (Y/N)", "Y", "N")
    {

    }

    // Update is called once per frame
    public override void CallProgress()
    {
        Status = 2;
        TargetObj.SetActive(true);
    }

    void Update()
    {
        if (Status == 2)
        {
            if (Input.anyKeyDown)
            {
                BaseText = " ";
                Status = 3;
                TypeTarget.InitTypeText();
                StartCoroutine(LoadNewLine());
            }
        }
    }    
    
}
