﻿using UnityEngine;
using System.Collections;

public class C1P09 : StoryBase
{

    // Use this for initialization
    public C1P09() : base("The ritual is complete.", "Y", "N")
    {

    }

    // Update is called once per frame
    public override void CallProgress()
    {
        Status = 2;
    }

    void Update()
    {
        if (Status == 2)
        {
            if (Input.anyKeyDown)
            {
                MusicTarget.GetComponent<MusicManager>().StopMusic();
                MusicTarget.GetComponent<MusicManager>().PlayEfc(3);
                // BaseText = "[footsteps]";
                Status = 3;
                // TypeTarget.InitTypeText();
                StartCoroutine(LoadNewLine());
            }
        }
    }

}
