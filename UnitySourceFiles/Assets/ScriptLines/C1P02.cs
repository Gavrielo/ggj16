﻿using UnityEngine;
using System.Collections;

public class C1P02 : StoryBase
{

    // Use this for initialization
    public C1P02() : base("Are you alone? (Y/N)", "Y", "N")
    {

    }

    // Update is called once per frame
    public override void CallProgress()
    {
        //TargetObj.GetComponent<MusicManager>().PlayEfc(3);
        Status = 2;
    }

    void Update()
    {
        if (Status == 2)
        {
            if (Input.GetKeyDown(KeyCode.Y))
            {
                BaseText = " Are you sure?";
                Status = 3;
                TypeTarget.InitTypeText();
                StartCoroutine(LoadNewLine());
            }
            else if (Input.GetKeyDown(KeyCode.N))
            {
                BaseText = " How perceptive...";
                Status = 3;
                TypeTarget.InitTypeText();
                StartCoroutine(LoadNewLine());
            }
        }

    }

    
}
