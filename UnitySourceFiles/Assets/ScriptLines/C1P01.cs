﻿using UnityEngine;
using System.Collections;

public class C1P01 : StoryBase
{

    // Use this for initialization
    public C1P01() : base("You've unleashed something you don't understand. Answer my        questions and you will survive.    Press any key if you understand.", "null", "null")
    {

    }
    public override void StartOfLine()
    {
        
    }

    // Update is called once per frame
    public override void CallProgress()
    {
       // BaseText = "";
       
        Status = 2;
        //TypeTarget.InitTypeText();
    }

    void Update()
    {
        if(Status==2)
        {
            if (Input.anyKeyDown)
            { 
                Debug.Log("A key or mouse click has been detected");
                Status = 3;

                StartCoroutine(LoadNewLine());
            }
        }

    }
    
}
