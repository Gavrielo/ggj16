﻿using UnityEngine;
using System.Collections;

public class C1P06 : StoryBase
{

    // Use this for initialization
    public C1P06() : base("Can you remember a time you     were afraid for your life? (Y/N)", "Y", "N")
    {

    }

    // Update is called once per frame
    public override void CallProgress()
    {
        Status = 2;
    }

    void Update()
    {
        if (Status == 2)
        {
            if (Input.GetKeyDown(KeyCode.Y))
            {
                BaseText = " You need to recall it. Relive it now.";
                Status = 3;
                TypeTarget.InitTypeText();
                StartCoroutine(LoadNewLine());
            }
            else if (Input.GetKeyDown(KeyCode.N))
            {
                BaseText = " Recall a time when you were        afraid. When you were paralyzed with fear.";
                Status = 3;
                TypeTarget.InitTypeText();
                StartCoroutine(LoadNewLine());
            }
        }

    }


}
