﻿using UnityEngine;
using System.Collections;

public class C1P03 : StoryBase
{

    // Use this for initialization
    public C1P03() : base("Are you afraid of the dark? (Y/N)", "Y", "N")
    {

    }

    // Update is called once per frame
    public override void CallProgress()
    {
        Status = 2;
    }

    void Update()
    {
        if (Status == 2)
        {
            if (Input.GetKeyDown(KeyCode.Y))
            {
                BaseText = " You might want to shut your eyes then.";
                SFXTarget.GetComponent<SFXManager>().PlayEfc(0);
                MusicTarget.GetComponent<MusicManager>().PlayEfc(1);
                Status = 3;
                TargetObj.GetComponent<Light>().enabled = false;
                TypeTarget.InitTypeText();
                StartCoroutine(LoadNewLine());
            }
            else if (Input.GetKeyDown(KeyCode.N))
            {
                BaseText = " Good.";
                MusicTarget.GetComponent<MusicManager>().PlayEfc(1);
                Status = 3;
                TypeTarget.InitTypeText();
                TargetObj.GetComponent<Light>().enabled = false;
                StartCoroutine(LoadNewLine());
            }
        }

    }

    
}
