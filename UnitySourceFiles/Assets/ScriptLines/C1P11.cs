﻿using UnityEngine;
using System.Collections;

public class C1P11 : StoryBase
{

    // Use this for initialization
    public C1P11() : base(" ", "Y", "N")
    {

    }

    // Update is called once per frame
    public override void CallProgress()
    {
        Status = 2;
    }

    void Update()
    {
        if (Status == 2)
        {
            if (Input.anyKeyDown)
            {
                // BaseText = "[footsteps]";
                Status = 3;
                // TypeTarget.InitTypeText();
                StartCoroutine(LoadNewLine());
            }
        }
    }

}
