﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class C1P10 : StoryBase
{
    public AnimationClip fadeAlphaAnimationClip;        //Animation clip fading out UI elements alpha
    public Animator animMenuAlpha;

    // Use this for initialization
    public C1P10() : base("I rise once more", "Y", "N")
    {

    }

    // Update is called once per frame
    public override void CallProgress()
    {
        TargetObj.SetActive(true);
        StartCoroutine(FadeOut());       
        Status = 2;
    }

    IEnumerator FadeOut()
    {
        print(Time.time);
        yield return new WaitForSeconds(2);
        animMenuAlpha.SetTrigger("fade");
        StartCoroutine(LoadNextScene());
    }

    IEnumerator LoadNextScene()
    {
        print(Time.time);
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene(1);
        print(Time.time);
    }



}
