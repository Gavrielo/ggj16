﻿using UnityEngine;
using System.Collections;

public class SFXManager : MonoBehaviour {

    public AudioClip[] SFX;

    void Start () {


    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void PlayEfc(int SFXNum)
    {
        GetComponent<AudioSource>().clip = SFX[SFXNum];
        GetComponent<AudioSource>().Play();
    }
}
