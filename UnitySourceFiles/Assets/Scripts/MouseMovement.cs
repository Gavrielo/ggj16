﻿using UnityEngine;
using System.Collections;

public class MouseMovement : MonoBehaviour
{
    public Vector2 MousePos = new Vector2(0,0);
    private float clampedY = 0f;
    private float clampedX = 0f;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        MousePos = new Vector2(Input.mousePosition.x-512, Input.mousePosition.y-384);
        // temp.z = 10f; // Set this to be the distance you want the object to be placed in front of the camera.
        // Vector3 lockLimitPos = new Vector3(temp.x, 1.5f, temp.y);
        clampedY = Mathf.Clamp((0.335f + (MousePos.y / 400f)), 0.18f,0.45f);
        clampedX = Mathf.Clamp(0.728f + ((MousePos.x / 400f)), 0.292f, 1.156f);
    
        this.transform.position = new Vector3(clampedX, 1.055f, clampedY);
        
        
    }

}
