﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AutoType : MonoBehaviour
{
    public float letterPause = 0.2f;
    public AudioClip sound;
    public StoryBase[] arrayOfLines;
    string message;
    public Text MainText;
    public int StoryPos=0;
    public bool JustStarted = true;
    public bool IsTyping = false;

    // Use this for initialization
    void Start()
    {
        MainText = GetComponent<Text>();
        message = arrayOfLines[0].GetComponent<StoryBase>().BaseText;
        MainText.text = "";
        StartCoroutine(TypeText());
    }

    IEnumerator TypeText()
    {
        IsTyping = true;
        if (JustStarted)
        {
            yield return new WaitForSeconds(1f);
            JustStarted = false;
        }
        foreach (char letter in message.ToCharArray())
        {
            MainText.text += letter;
            if (sound)
                GetComponent<AudioSource>().PlayOneShot(sound);
            yield return 0;
            yield return new WaitForSeconds(letterPause);
            if(MainText.text == message && arrayOfLines[StoryPos].GetComponent<StoryBase>().Status==1)
            {
                if(MainText.text == message)
                    IsTyping = false;
                yield return new WaitForSeconds(1f);
                Debug.Log("Complete");
                arrayOfLines[StoryPos].GetComponent<StoryBase>().CallProgress();
                
            }
        }
    }

    public void InitTypeText()
    {
        message = arrayOfLines[StoryPos].GetComponent<StoryBase>().BaseText;
        MainText.text = "";
        StartCoroutine(TypeText());
    }
}