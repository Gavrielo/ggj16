﻿using UnityEngine;
using System.Collections;

public class MusicManager : MonoBehaviour {

    public AudioClip[] music;
    public int PlayHowManyTimes = 0;

	void Start () {

        //GetComponent<AudioSource>().loop = true;
        // GetComponent<AudioSource>().PlayOneShot(music[0]);

        GetComponent<AudioSource>().clip = music[0];
        GetComponent<AudioSource>().Play();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void PlayEfc(int TrackNum)
    {
        GetComponent<AudioSource>().clip = music[TrackNum];
        GetComponent<AudioSource>().Play();
    }
    

    public void StopMusic()
    {
        GetComponent<AudioSource>().Stop();

    }
}
