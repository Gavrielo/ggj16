﻿using UnityEngine;
using System.Collections;

public class Fade : MonoBehaviour
{

    public AnimationClip fadeAlphaAnimationClip;        //Animation clip fading out UI elements alpha
    public Animator animMenuAlpha;

    // Use this for initialization
    void Start()
    {
        animMenuAlpha.SetTrigger("fade");
    }

    // Update is called once per frame
    void Update()
    {

    }
}
